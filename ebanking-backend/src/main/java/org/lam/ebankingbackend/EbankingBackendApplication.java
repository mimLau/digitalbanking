package org.lam.ebankingbackend;

import org.lam.ebankingbackend.dtos.BankAccountDTO;
import org.lam.ebankingbackend.dtos.CurrentBankAccountDTO;
import org.lam.ebankingbackend.dtos.CustomerDTO;
import org.lam.ebankingbackend.dtos.SavingBankAccountDTO;
import org.lam.ebankingbackend.entities.BankAccount;
import org.lam.ebankingbackend.entities.Customer;
import org.lam.ebankingbackend.entities.SavingBankAccount;
import org.lam.ebankingbackend.exceptions.BalanceNotSufficientException;
import org.lam.ebankingbackend.exceptions.BankAccountNotFoundException;
import org.lam.ebankingbackend.exceptions.CustomerNotFoundException;
import org.lam.ebankingbackend.services.BankAccountService;
import org.lam.ebankingbackend.services.BankAccountServiceImpl;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.stream.Stream;

@SpringBootApplication
public class EbankingBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(EbankingBackendApplication.class, args);
    }

    @Bean
    CommandLineRunner start(BankAccountServiceImpl bankService, BankAccountService bankAccountService) {

        return args -> {
               // bankService.consulter();

            Stream.of("Maryam", "Imane", "Mohamed").forEach(name-> {
                CustomerDTO customerDTO = new CustomerDTO();
                customerDTO.setName(name);
                customerDTO.setEmail(name + "@gmail.com");
                bankAccountService.saveCustomer(customerDTO);

            });

            bankAccountService.listCustomers().forEach(customer->{

                try {
                    bankAccountService.saveCurrentBankAccount(Math.random()*5400, 5000, customer.getId());
                    bankAccountService.saveSavingBankAccount(Math.random()*120000, 2.5, customer.getId());

                } catch (CustomerNotFoundException e) {
                    e.printStackTrace();
                }

            });


            List<BankAccountDTO> bankAccountList = bankAccountService.listBankAccounts();

            for (BankAccountDTO accountDTO : bankAccountList ) {
                for(int i = 0; i < 10 ; i++) {

                    String accountId;

                    if(accountDTO instanceof SavingBankAccountDTO) {
                        accountId = ((SavingBankAccountDTO) accountDTO).getId();

                    } else {
                        accountId = ((CurrentBankAccountDTO) accountDTO).getId();
                    }

                    bankAccountService.credit(accountId, 10000 + Math.random() * 120000, "credit");
                    bankAccountService.debit(accountId, 1000 + Math.random() * 9000, "Debit");
                }
            }



        };

    }

    /*@Bean
    CommandLineRunner start(CustomerRepository customerRepository,
                            AccountOperationRepository accountOperationtRepository,
                            BankAccountRepository bankAccountRepository) {
        return args -> {

            Stream.of("Hassan", "Yassine", "Aïcha").forEach(name ->  {
                Customer customer = new Customer();
                customer.setName(name);
                customer.setEmail(name + "@gmail.com");
                customerRepository.save(customer);
            });

            customerRepository.findAll().forEach(cust->{
                CurrentBankAccountDTO currentAccount = new CurrentBankAccountDTO();
                currentAccount.setId(UUID.randomUUID().toString());
                currentAccount.setBalance(Math.random()*90000);
                currentAccount.setCreatedAt(new Date());
                currentAccount.setStatus(AccountStatus.CREATED);
                currentAccount.setOverDraft(9000);
                currentAccount.setCustomer(cust);
                bankAccountRepository.save(currentAccount);

                SavingBankAccount savingAccount = new SavingBankAccount();
                savingAccount.setId(UUID.randomUUID().toString());
                savingAccount.setBalance(Math.random()*90000);
                savingAccount.setCreatedAt(new Date());
                savingAccount.setStatus(AccountStatus.CREATED);
                savingAccount.setInterestRate(5.2);
                savingAccount.setCustomer(cust);
                bankAccountRepository.save(savingAccount);
            });


            bankAccountRepository.findAll().forEach(acc -> {

                for (int i = 0; i < 10 ; i++) {

                    AccountOperation accountOperation = new AccountOperation();
                    accountOperation.setOperationDate(new Date());
                    accountOperation.setAmount(Math.random()*12000);
                    accountOperation.setType(Math.random() > 0.5 ? OperationType.DEBIT : OperationType.CREDIT );
                    accountOperation.setBankAccount(acc);
                    accountOperationtRepository.save(accountOperation);

                }

            });



        };
    }*/

}
