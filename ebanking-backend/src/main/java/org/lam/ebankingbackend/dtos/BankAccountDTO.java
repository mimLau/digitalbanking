package org.lam.ebankingbackend.dtos;

import lombok.Data;
import org.lam.ebankingbackend.enums.AccountStatus;

import java.util.Date;

@Data
public class BankAccountDTO {

    private String id;
    private double balance;
    private Date createdAt;
    private AccountStatus status;
    private CustomerDTO customer;
    private String type;
}
