package org.lam.ebankingbackend.entities;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@DiscriminatorValue("CU") // => ça va avec le strategy = InheritanceType.SINGLE_TABLE
@Data @NoArgsConstructor @AllArgsConstructor
public class CurrentBankAccount extends BankAccount {
    private double overDraft;
}
