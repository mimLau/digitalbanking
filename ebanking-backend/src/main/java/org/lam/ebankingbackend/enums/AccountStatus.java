package org.lam.ebankingbackend.enums;

public enum AccountStatus {
    CREATED, ACTIVATED, SUSPENDED
}
