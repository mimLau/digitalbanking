package org.lam.ebankingbackend.enums;

public enum OperationType {
    DEBIT, CREDIT
}
