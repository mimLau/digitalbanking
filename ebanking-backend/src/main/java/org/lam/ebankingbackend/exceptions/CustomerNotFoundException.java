package org.lam.ebankingbackend.exceptions;

/*public class CustomerNotFoundException extends Throwable {*/    // exception   surveillée
/*public class CustomerNotFoundException extends RuntimeException {*/ // exception non surveillée sans try catch ou throw
public class CustomerNotFoundException extends Exception {  // exception   surveillée faut ajouter try catch ou throws


    public CustomerNotFoundException(String message) {

        super(message);
    }
}
