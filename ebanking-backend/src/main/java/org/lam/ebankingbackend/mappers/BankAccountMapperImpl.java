package org.lam.ebankingbackend.mappers;

import org.lam.ebankingbackend.dtos.*;
import org.lam.ebankingbackend.entities.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class BankAccountMapperImpl {

    public CustomerDTO fromCustomer(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO();
        BeanUtils.copyProperties(customer, customerDTO);
        return customerDTO;
    }

    public Customer fromCustomerDTO(CustomerDTO customerDTO) {
        Customer customer = new Customer();
        BeanUtils.copyProperties(customerDTO, customer);
        return customer;
    }


    public SavingBankAccountDTO fromSavingAccount(SavingBankAccount savingBankAccount) {
        SavingBankAccountDTO savingBankAccountDTO = new SavingBankAccountDTO();
        BeanUtils.copyProperties(savingBankAccount, savingBankAccountDTO);
        savingBankAccountDTO.setCustomer(fromCustomer(savingBankAccount.getCustomer()));
        savingBankAccountDTO.setType(savingBankAccount.getClass().getSimpleName());

        return savingBankAccountDTO;
    }

    public SavingBankAccount fromSavingAccountDTO(SavingBankAccountDTO savingBankAccountDTO) {
        SavingBankAccount savingBankAccount = new SavingBankAccount();
        BeanUtils.copyProperties(savingBankAccountDTO, savingBankAccount);
        savingBankAccount.setCustomer(fromCustomerDTO(savingBankAccountDTO.getCustomer()));

        return savingBankAccount;
    }

    public CurrentBankAccountDTO fromCurrentBankAccount(CurrentBankAccount currentBankAccount) {
        CurrentBankAccountDTO currentBankAccountDTO = new CurrentBankAccountDTO();
        BeanUtils.copyProperties(currentBankAccount, currentBankAccountDTO);
        currentBankAccountDTO.setCustomer(fromCustomer(currentBankAccount.getCustomer()));
        currentBankAccountDTO.setType(currentBankAccount.getClass().getSimpleName());

        return currentBankAccountDTO;

    }

    public CurrentBankAccount fromCurrentBankAccountDTO(CurrentBankAccountDTO currentBankAccountDTO) {
        CurrentBankAccount currentBankAccount = new CurrentBankAccount();
        BeanUtils.copyProperties(currentBankAccountDTO, currentBankAccount);
        currentBankAccount.setCustomer(fromCustomerDTO(currentBankAccountDTO.getCustomer()));

        return currentBankAccount;
    }

    public AccountOperationDTO fromAccountOperation(AccountOperation accountOperation) {
        AccountOperationDTO accountOperationDTO = new AccountOperationDTO();
        BeanUtils.copyProperties(accountOperation, accountOperationDTO);

        return accountOperationDTO;
    }
}
