package org.lam.ebankingbackend.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lam.ebankingbackend.dtos.*;
import org.lam.ebankingbackend.entities.*;
import org.lam.ebankingbackend.enums.OperationType;
import org.lam.ebankingbackend.exceptions.BalanceNotSufficientException;
import org.lam.ebankingbackend.exceptions.BankAccountNotFoundException;
import org.lam.ebankingbackend.exceptions.CustomerNotFoundException;
import org.lam.ebankingbackend.mappers.BankAccountMapperImpl;
import org.lam.ebankingbackend.repositories.AccountOperationRepository;
import org.lam.ebankingbackend.repositories.BankAccountRepository;
import org.lam.ebankingbackend.repositories.CustomerRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional  // pour éviter les exception de type lazy
@AllArgsConstructor
@Slf4j //loguer les messages pour faire de la journalisation
public class BankAccountServiceImpl implements BankAccountService {

    private CustomerRepository customerRepository;
    private BankAccountRepository bankAccountRepository;
    private AccountOperationRepository accountOperationRepository;
    private BankAccountMapperImpl dtoMapper;



    /*public void consulter() {

        BankAccount bankAccount = bankAccountRepository.findById("1b38dcc7-830b-434e-8f84-c90e48712c87")
                .orElse(null);

        if(bankAccount != null) {
            System.out.println("*********************************************************");
            System.out.println(bankAccount.getId());
            System.out.println(bankAccount.getBalance());
            System.out.println(bankAccount.getStatus());
            System.out.println(bankAccount.getCreatedAt());
            System.out.println(bankAccount.getCustomer().getName());

            System.out.println(bankAccount.getClass().getSimpleName());

            if(bankAccount instanceof CurrentBankAccountDTO) {
                System.out.println("Over Draft => " +  ((CurrentBankAccountDTO)bankAccount).getOverDraft());

            } else if (bankAccount instanceof SavingBankAccount) {
                System.out.println("Rate => " +  ((SavingBankAccount)bankAccount).getInterestRate());
            }

            bankAccount.getAccountOperations().forEach(op->{
                System.out.println(op.getType() + "\t" + op.getOperationDate() + "\t" + op.getAmount());

            });

        }

    }*/


    @Override
    public CustomerDTO getCustomer(Long customerId) throws CustomerNotFoundException {
        Customer customer = customerRepository.findById(customerId).orElseThrow(() -> new CustomerNotFoundException("Customer not found"));

        return dtoMapper.fromCustomer(customer);
    }

    @Override
    public CustomerDTO saveCustomer(CustomerDTO customerDTO) {

        log.info("Saving new customer");
        Customer customer = dtoMapper.fromCustomerDTO(customerDTO);
        Customer savedCustomer = customerRepository.save(customer);
        return dtoMapper.fromCustomer(savedCustomer);

    }
    @Override
    public CustomerDTO updateCustomer(CustomerDTO customerDTO) {

        log.info("Updating new customer");
        Customer customer = dtoMapper.fromCustomerDTO(customerDTO);
        Customer savedCustomer = customerRepository.save(customer);
        return dtoMapper.fromCustomer(savedCustomer);

    }

    @Override
    public void deleteCustomer(Long customerId) {
        customerRepository.deleteById(customerId);
    }


    @Override
    public CurrentBankAccountDTO saveCurrentBankAccount(double initialBalance, double overDraft, Long customerId) throws CustomerNotFoundException {

        Customer customer = customerRepository.findById(customerId).orElse(null);

        if(customer == null) {
            throw new CustomerNotFoundException("Customer not found");
        }

        CurrentBankAccount currentBankAccount = new CurrentBankAccount();
        currentBankAccount.setId(UUID.randomUUID().toString());
        currentBankAccount.setCreatedAt(new Date());
        currentBankAccount.setBalance(initialBalance);
        currentBankAccount.setOverDraft(overDraft);
        currentBankAccount.setCustomer(customer);
        CurrentBankAccount savedBankAccount = bankAccountRepository.save(currentBankAccount);

        return dtoMapper.fromCurrentBankAccount(savedBankAccount);

    }

    @Override
    public SavingBankAccountDTO saveSavingBankAccount(double initialBalance, double interestRate, Long customerId) throws CustomerNotFoundException {
        Customer customer = customerRepository.findById(customerId).orElse(null);

        if(customer == null) {
            throw new CustomerNotFoundException("Customer not found");
        }

        SavingBankAccount savingBankAccount = new SavingBankAccount();
        savingBankAccount.setId(UUID.randomUUID().toString());
        savingBankAccount.setCreatedAt(new Date());
        savingBankAccount.setBalance(initialBalance);
        savingBankAccount.setInterestRate(interestRate);
        savingBankAccount.setCustomer(customer);
        SavingBankAccount savedBankAccount = bankAccountRepository.save(savingBankAccount);

        return dtoMapper.fromSavingAccount(savedBankAccount);
    }




    @Override
    public List<CustomerDTO> listCustomers() {

        List<Customer> customers = customerRepository.findAll();

        // Programation impérative
        /*List<CustomerDTO> customerDTOS = new ArrayList<>();
        for(Customer customer : customers) {

            CustomerDTO customerDTO = dtoMapper.fromCustomer(customer);
            customerDTOS.add(customerDTO);
        }*/

        // Programation fonctionnelle
        List<CustomerDTO> customerDTOS = customers.stream().map(cust -> dtoMapper.fromCustomer(cust)).collect(Collectors.toList());

        return customerDTOS;

    }

    @Override
    public List<BankAccountDTO> listBankAccounts() {
        List<BankAccount> bankAccounts = bankAccountRepository.findAll();

        List<BankAccountDTO> bankAccountDTOS = bankAccounts.stream().map(bankAcc -> {
            if (bankAcc instanceof SavingBankAccount) {
                return dtoMapper.fromSavingAccount((SavingBankAccount) bankAcc);
            } else {
                return dtoMapper.fromCurrentBankAccount((CurrentBankAccount) bankAcc);
            }

        }).collect(Collectors.toList());

        return bankAccountDTOS;
    }

    @Override
    public BankAccountDTO getBankAccount(String accountId) throws BankAccountNotFoundException {
        BankAccount bankAccount = getAccount(accountId);

        if(bankAccount instanceof SavingBankAccount) {
            SavingBankAccount savingBankAccount = (SavingBankAccount) bankAccount;
            return dtoMapper.fromSavingAccount(savingBankAccount);
        } else {
            CurrentBankAccount currentBankAccount = (CurrentBankAccount) bankAccount;
            return dtoMapper.fromCurrentBankAccount(currentBankAccount);
        }

    }


    @Override
    public void debit(String accountId, double amount, String description) throws BankAccountNotFoundException, BalanceNotSufficientException {

        BankAccount bankAccount = getAccount(accountId);

        if(bankAccount.getBalance()<amount)
            throw new BalanceNotSufficientException("Balance not sufficient");

        AccountOperation accountOperation = new AccountOperation();
        accountOperation.setType(OperationType.DEBIT);
        accountOperation.setAmount(amount);
        accountOperation.setDescription(description);
        accountOperation.setOperationDate(new Date());
        accountOperation.setBankAccount(bankAccount);

        accountOperationRepository.save(accountOperation);
        bankAccount.setBalance(bankAccount.getBalance()-amount);
        bankAccountRepository.save(bankAccount);

    }

    @Override
    public void credit(String accountId, double amount, String description) throws BankAccountNotFoundException {

        BankAccount bankAccount = getAccount(accountId);

        AccountOperation accountOperation = new AccountOperation();
        accountOperation.setType(OperationType.CREDIT);
        accountOperation.setAmount(amount);
        accountOperation.setDescription(description);
        accountOperation.setOperationDate(new Date());
        accountOperation.setBankAccount(bankAccount);

        accountOperationRepository.save(accountOperation);
        bankAccount.setBalance(bankAccount.getBalance()+amount);
        bankAccountRepository.save(bankAccount);

    }

    @Override
    public void transfer(String accountIdSource, String accountIdDestination, double amount) throws BankAccountNotFoundException, BalanceNotSufficientException {

        debit(accountIdSource, amount, "Transfer to " + accountIdDestination);
        credit(accountIdDestination, amount, "Transfer from " + accountIdSource);

    }


    @Override
    public List<AccountOperationDTO> accountOperationsList(String accountId) {
        List<AccountOperation> accountOperations = accountOperationRepository.findByBankAccountId(accountId);

       return  accountOperations.stream().map(op -> dtoMapper.fromAccountOperation(op)).collect(Collectors.toList());

    }

    @Override
    public AccountHistoryDTO getAccountHistory(String accountId, int page, int size) throws BankAccountNotFoundException {

        BankAccount bankAccount =  bankAccountRepository.findById(accountId).orElse(null);

        if(bankAccount == null) throw new BankAccountNotFoundException("Account not found");

        Page<AccountOperation> accountOperations = accountOperationRepository.findByBankAccountId(accountId, PageRequest.of(page, size));
        AccountHistoryDTO accountHistoryDTO = new AccountHistoryDTO();
        List<AccountOperationDTO> accountOperationDTOS = accountOperations.getContent().stream().map(op -> dtoMapper.fromAccountOperation(op)).collect(Collectors.toList());
        accountHistoryDTO.setAccountOperationDTOS(accountOperationDTOS);
        accountHistoryDTO.setAccountId(bankAccount.getId());
        accountHistoryDTO.setBalance(bankAccount.getBalance());
        accountHistoryDTO.setCurrentPage(page);
        accountHistoryDTO.setPageSize(size);
        accountHistoryDTO.setTotalPages(accountOperations.getTotalPages());


        return accountHistoryDTO;
    }


    private BankAccount getAccount(String accountId) throws BankAccountNotFoundException {
        return bankAccountRepository.findById(accountId)
                .orElseThrow(()-> new BankAccountNotFoundException("BankAccount not found"));
    }


}
